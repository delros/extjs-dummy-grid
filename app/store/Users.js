Ext.define('AppExt01.store.Users', {
  extend: 'Ext.data.Store',
  fields: ['id', 'name', 'email', 'istest', 'country', 'created'],
  model: 'AppExt01.model.User',
  data: [
    {
      id: 1001,
      name: 'del Rosario Ernesto',
      email: 'delrosario.ernesto@gmail.com',
      istest: 0,
      country: 'Ukraine'
    },
    {
      id: 1002,
      name: 'del Rosario Vitalina',
      email: 'delrosario.vitalinka@gmail.com',
      istest: 1
    }, {
      id: 1020,
      name: 'Travolta John',
      email: 'travolta.j@gmail.com',
      istest: 0,
      country: 'USA'
    }
  ]
});