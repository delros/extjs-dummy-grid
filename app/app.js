// CDN Ext.Loader configuration
Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath(
  "Ext.ux", 
  "http://cdn.sencha.io/ext-4.0.7-gpl/examples/ux"
);

Ext.require('Ext.container.Viewport');

Ext.application({
  name: 'AppExt01',
  appFolder: 'app',
  controllers: [
    'Users'
  ],
  launch: function() {
    Ext.create('Ext.container.Viewport', {
      layout: 'fit',
      defaults : {
        padding: '10'
      },
      items: [
        {
          xtype: 'userlist',
          title: 'Users'
        }
      ]
    });
  }
});