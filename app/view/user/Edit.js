Ext.define('AppExt01.view.user.Edit', {
  
  extend: 'Ext.window.Window',
  alias : 'widget.useredit',
  title : 'Edit User',
  layout: 'fit',
  autoShow: true,
  defaults: {
    bodyStyle: 'padding:10px'
  },
  buttons: [
    {
      text: 'Save',
      action: 'save'
    }
  ],

  initComponent: function() {
    this.items = [
      {
        xtype: 'form',
        items: [
          {
            xtype: 'hiddenfield',
            name: 'id'
          }, {
            xtype: 'textfield',
            name : 'name',
            fieldLabel: 'Name'
            // allowBlank: false
          }, {
            xtype: 'textfield',
            name : 'email',
            fieldLabel: 'Email',
            // allowBlank: false,
            vtype: 'email'
          }, {
            xtype: 'radiogroup',
            fieldLabel: 'Is test user?',
            columns: 1,
            vertical: true,
            items: [
              { 
                boxLabel: 'No', 
                name: 'istest', 
                inputValue: 0, 
                checked: true 
              }, { 
                boxLabel: 'Yes', 
                name: 'istest', 
                inputValue: 1
              }
            ],
            listeners: {
              scope: this,
              change: {
                fn: this.toggleAdditionalFields
              }
            }
          }, {
            xtype: 'textfield',
            name : 'country',
            fieldLabel: 'Country'
           }, {
            xtype: 'datefield',
            name : 'created',
            fieldLabel: 'Created on',
            maxDate: new Date()
          }
        ]
      }
    ];

    this.callParent(arguments);
  },
  toggleAdditionalFields: function(self, newValue, oldValue) {
    var form = this.down('form').getForm(),
        fields = ['country', 'created'];

    for (var i in fields) {
      form.findField(fields[i]).setVisible(!newValue.istest);
    }
  }

});