Ext.define('AppExt01.view.user.List', {
  
  extend: 'Ext.grid.Panel',
  alias : 'widget.userlist',
  title : 'All Users',
  store: 'Users',
  dockedItems: [{
    xtype: 'pagingtoolbar',
    store: 'Users',
    dock: 'bottom',
    displayInfo: true
  }],

  initComponent: function() {

    var button = Ext.create('Ext.button.Button', { 
      text: 'Create new user',
      icon: 'resources/img/add.png',
      handler : function (){
        var view = Ext.widget('usercreate')
        view.down('form');
        console.log('Add button pressed');
      }
    });    

    this.tbar = [
      button
    ];

    this.columns = [
      {
        header: 'ID',
        dataIndex: 'id',
        flex: 0
      }, { 
        header: 'Name', 
        dataIndex: 'name',  
        flex: 1 
      }, { 
        header: 'Email', 
        dataIndex: 'email', 
        flex: 1 
      }, { 
        xtype: 'booleancolumn', 
        header: 'Is test user?', 
        dataIndex: 'istest', 
        flex: 1,
        trueText: 'Yes',
        falseText: 'No' 
      }, {
        header: 'Action',
        xtype: 'actioncolumn',
        width: 50,
        sortable: false,
        items: [{
          icon   : 'resources/img/delete.gif',
          tooltip: 'Remove entry',
          handler: function(grid, rowIndex, colIndex) {
            var store = grid.getStore(),
                record = store.getAt(rowIndex);  

            store.remove(record);
          }
        }]
      }      
    ];

    this.callParent(arguments);
  }
  
});