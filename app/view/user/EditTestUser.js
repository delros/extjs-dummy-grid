Ext.define('AppExt01.view.user.EditTestUser', {
  
  extend: 'Ext.window.Window',
  alias : 'widget.usertestedit',
  title : 'Edit Test User',
  layout: 'fit',
  autoShow: true,

  initComponent: function() {
    this.items = [
      {
        xtype: 'form',
        items: [
          {
            xtype: 'textfield',
            name : 'name',
            fieldLabel: 'Name'
          }, {
            xtype: 'textfield',
            name : 'email',
            fieldLabel: 'Email'
          }, {
            xtype : 'fieldcontainer',
            fieldLabel : 'Is test user?',
            defaultType: 'radiofield',
            items: [
              {
                boxLabel: 'No',
                name: 'istest',
                inputValue: 0,
                id: 'istestNo'
              }, {
                boxLabel: 'Yes',
                name: 'istest',
                inputValue: 1,
                id: 'istestYes'
              }
            ]
          }
        ]
      }
    ];

    this.buttons = [
      {
        text: 'Save',
        action: 'save'
      }, {
        text: 'Cancel',
        scope: this,
        handler: this.close
      }
    ];

    this.callParent(arguments);
  }

});