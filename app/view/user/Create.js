Ext.define('AppExt01.view.user.Create', {
  
  extend: 'AppExt01.view.user.Edit',
  alias : 'widget.usercreate',
  title : 'Create New User',
  layout: 'fit',
  autoShow: true,
  defaults: {
    bodyStyle: 'padding:10px'
  },
  buttons: [
    {
      text: 'Create',
      action: 'create'
    }
  ]

});