Ext.define('AppExt01.model.User', {
  extend: 'Ext.data.Model',
  fields: ['name', 'email', 'istest', 'created', 'country']
});