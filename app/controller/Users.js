Ext.define('AppExt01.controller.Users', {
  
  extend: 'Ext.app.Controller',
  views: [
    'user.List',
    'user.Edit',
    'user.EditTestUser',
    'user.Create'
  ],
  stores: [ 'Users' ],
  models: [ 'User' ],
  
  init: function() {
    this.control({
      'useredit button[action=save], usertestedit button[action=save]': {
        click: this.updateUser
      },
      'usercreate button[action=create]': {
        click: this.saveUser
      },
      'userlist': {
        itemdblclick: this.editUser
      }
    });
  },
  editUser: function(grid, record) {
    var view,
        isTestUser = record.get('istest');

    // if (isTestUser) {
    //   view = Ext.widget('usertestedit')
    // } else {
      view = Ext.widget('useredit')
    // }

    view.down('form').loadRecord(record);
  },
  addUser: function(grid, record) {
    var view = Ext.widget('useredit')
    view.down('form');
  },  
  updateUser: function(button) {
    var win = button.up('window'),
        form = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();

    record.set(values);
    win.close();
  },
  saveUser: function(button) {
    var win = button.up('window'),
        form = win.down('form'),
        values = form.getValues(),
        store = Ext.data.StoreManager.lookup('Users');

    if (!values.id) {
      values.id = store.max('id') + 1;
    }

    var user = new AppExt01.model.User(values);

    store.add(user);
    store.commitChanges();

    win.close();
    console.log('New user saved', store.getCount());
  }

});